<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            1 => [
                'title' => 'Адмін',
            ],
            2 => [
                'title' => 'Розробник',
            ]
        ];

        DB::beginTransaction();
        try{

            foreach ($roles as $id => $role) {
                Role::updateOrCreate(
                    ['id' => $id],
                    $role
                );
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
        }
    }
}
