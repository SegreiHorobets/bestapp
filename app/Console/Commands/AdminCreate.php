<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class AdminCreate extends Command
{
    protected $signature = 'user:create:admin {name=Admin}';
    protected $description = 'Administrator created command';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $user = new User();
        $userEmail = $this->ask('send email for administrator');

        if(!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
            $this->error('Неверный формат email');
            exit();
        }

        if(!$user->where('email', $userEmail)->exists()) {
            $userPassword = $this->secret("send admin's password");

            if(empty($userPassword)) {
                $this->error('Password is empty');
                exit();
            }

            if($this->secret('Confirm your password') == $userPassword) {
                $user->email = $userEmail;
                $user->role_id = Role::find(1)->id;
                $user->password = bcrypt($userPassword);

                if($user->save()) {
                    $message = "User $user->name is created!";
                    $this->info($message);
                }
            } else {
                $this->error('Password error.');
            }
        } else {
            $this->error('Email alredy in use!');
        }
    }
}
