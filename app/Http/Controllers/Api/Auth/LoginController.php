<?php

namespace App\Http\Controllers\Api\Auth;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as PassportClient;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    protected $client;

    /**
     * AdminLoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        $client = new PassportClient;
        $this->client = $client->where('password_client', 1)->first();
        if (!$this->client) {
            abort(400, __('system.passport'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::guard()->attempt($credentials)){
            $params = [
                'grant_type'    => 'password',
                'client_id'     => $this->client->id,
                'client_secret' => $this->client->secret,
                'username'      => trim($request->email),
                'password'      => trim($request->password),
                'scope'         => '*',
            ];

            $http = new Client();

            $response = $http->post(url('/oauth/token'), [
                'form_params' => $params
            ]);

            $response = json_decode($response->getBody()->getContents());
            $response->role_id = Auth::user()->role_id;
            $response->user_id = Auth::user()->id;
            $response = json_encode($response);

            return $response;
        }

        return response()->json([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'password' => [
                    'The selected password is invalid.'
                ]
            ]
        ], 422);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = Auth::guard()->user();

        if (null !== $user) {
            $access_token = $user->token();

            if (null !== $access_token) {
                DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $access_token->id)
                    ->update(['revoked' => true]);

                $access_token->revoke();

                return response()->json([], 200);
            }

            $request->session()->invalidate();

            return response()->json([], 200);
        }

        return response()->json([], 200);

    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|mixed
     */
    public function refreshToken(Request $request)
    {
        $http = new Client();

        try {

            $response = Http::asForm()->post(url('/oauth/token'), [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id'     => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '',
            ]);

            return $response->json();

        }catch (\Exception $e){
            return response()->json([],401);
        }

        return response()->json([],401);
    }
}
